<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'long_url',
    ];

    /**
     * Get the clicks for this url.
     */
    public function hits()
    {
        return $this->hasMany('App\UrlHit');
    }

    /**
     * Create the short link to return to the view.
     */
    public function getLink(int $id)
    {
        $root = url('/');

        $link = "<a href=$root/shorturl/$id target=_blank>$root/shorturl/$id</a>";

        return $link;
    }
}
