<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UrlHit as UrlHit;
use App\Url as URL;
use Redirect;

class UrlHitController extends Controller
{
    /**
     * Catch hits for each short url.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$url = Url::with('hits')->findOrFail($request->id);
    	$url->hits()->save(new UrlHit());

      	return redirect()->away($url->long_url);
    }
}
