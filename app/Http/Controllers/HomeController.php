<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Url as Url;
use App\Http\Requests\UrlRequest;

class HomeController extends Controller
{
    /**
     * Show the URL shortener.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Return a short url from any url input
     *
     * @param  App\Http\Requests\UrlRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UrlRequest $request)
    {
        $newUrl = new Url();

        $url = trim($request->url);

        $existingUrl = Url::where('long_url', '=', $url)->first();

        //if the url doesn't exist
       if ($existingUrl == null) {
            $newUrl->long_url = $url;
            $newUrl->save();
            $result = $newUrl->getLink($newUrl->id);

        } else {
           $result = $existingUrl->getLink($existingUrl->id);
        }

        return view('home')->with('result', $result);
    }
}
