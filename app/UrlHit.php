<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlHit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
    /**
     * Get the url that haeen hit
     */
    public function url()
    {
        return $this->belongsTo('App\Url');
    }
}
