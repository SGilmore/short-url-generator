<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Querystring implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return  (strpos($value, 'p=') !== false) && 
                (strpos($value, 'awinaffid=') !== false) &&
                (strpos($value, 'awinid=') !== false);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The url must contain the following parameters: p, awinid, awinaffid';
    }
}
