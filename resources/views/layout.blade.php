<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>URL Shortener</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
        <script src="{{ mix('js/app.js') }}" async defer></script>
    </head>

    <body>
        <header>
            @include('partials.header')
        </header>
        <main class="content">
            <div class="container">
                @yield('content')
            </div>
        </main>
        <footer>
            @include('partials.footer')
        </footer>
    </body>

</html>