@extends('layout')

@section('content')
    <h1>URL Shortener</h1>
    <div class="row">
		<div class="col-sm-12">

		    <p>Use this tool to shorten a long URL</p>

		    <form method="POST" action="/" id="url_shortener">
		    	@csrf
		    	
				<div class="form-group">
					<label for="url">Enter your URL</label>

					<input
						type="url"
						class="form-control"
						id="url"
						name="url"
						aria-describedby="url"
						placeholder="Enter your URL">
				</div>
				@if($errors->has('url'))
					<div class="alert alert-danger" role="alert">
						{{ $errors->first('url') }}
					</div>
				@endif

				<button type="submit" class="btn btn-primary">Generate</button>
			</form>

			<div id="results" class="content">
				<h2>Your new URL is&hellip;</h2>
                <p>{!! $result ?? 'No result found' !!}</p>
			</div>
		</div>
	</div>
@endsection